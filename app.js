const express = require('express')
const api = express()
const apiRoute = require('./route/api')


api.use('/',apiRoute.api)

module.exports={
    api
}