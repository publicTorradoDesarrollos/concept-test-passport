
const app = require('./app')

const dotenv = require('dotenv').config()
const port = process.env.PORT


app.api.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})