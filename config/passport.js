const passport = require('passport')
var JwtStrategy = require('passport-jwt').Strategy
var ExtractJwt = require('passport-jwt').ExtractJwt;

const dotenv = require('dotenv').config()
var sercretJWWT = process.env.SECRET_JWT

var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = sercretJWWT;

passport.use(new JwtStrategy(opts, function(jwt_payload, done) {

    try{
        return done(null, jwt_payload);
      } catch (error) {
        return done(error,false);
      }


}));


module.exports={
    passport
}