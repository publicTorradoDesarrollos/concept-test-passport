const express = require('express')
const api = express.Router()
const controllerTest = require('../controllers/api/test')
const middlewareAuth =require('../middlewares/auth')
const configPassport = require('../config/passport')



api.get('/test', controllerTest.test)

api.post('/test', configPassport.passport.authenticate('jwt', { session: false }), controllerTest.test)



module.exports = {
    api
}