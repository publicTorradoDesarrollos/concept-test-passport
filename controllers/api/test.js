const dotenv = require('dotenv').config()
const jwt = require('jwt-simple')
const moment = require('moment')



function test (req,res){
    var sercretJWWT = process.env.SECRET_JWT
    const payload = {
        // En el payload va la información del dispositivo
        sub: "test",
        iat: moment().unix(),
        exp: moment().add(30000,'seconds').unix()
      }
      res.send(jwt.encode(payload,sercretJWWT))
 }

 module.exports={
    test
 }